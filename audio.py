from pydub import AudioSegment
from pydub.playback import play
from pathlib import Path
import random
import os
import shutil
# Mostly background noise
# https://www.pacdv.com/sounds/voices-1.html

#alternative source
#https://www.thesoundarchive.com/

MYPATH = Path().absolute()

#Clears the AudioOutput folder ready for new outputs
for root, dirs, files in os.walk(str(MYPATH) + "/AudioOutput/"):
    for f in files:
        os.unlink(os.path.join(root, f))
    for d in dirs:
        shutil.rmtree(os.path.join(root, d))

n = 0
while n < 20:

    n1 = random.randint(1, 20)
    n2 = random.randint(1, 20)
    while n1 == n2:
        n1 = random.randint(1, 20)
        n2 = random.randint(1, 20)

    if n1 > n2:
        w = n1
        n1 = n2
        n2 = w

    while os.path.exists("/AudioOutput/" + str(n1) + "_" + str(n2) + ".wav") or os.path.exists("/AudioOutput/" + str(n2) + "_" + str(n1) + ".wav"):
        n1 = random.randint(1, 20)
        n2 = random.randint(1, 20)
        while n1 == n2:
            n1 = random.randint(1, 20)
            n2 = random.randint(1, 20)

    sound1_string = str(n1) + ".wav"
    sound2_string = str(n2) + ".wav"

    sound1 = AudioSegment.from_file("AudioSamples/" + sound1_string)
    sound2 = AudioSegment.from_file("AudioSamples/" + sound2_string)


    SOUNDS_PATH = str(MYPATH)
    combined = sound1.overlay(sound2)

    combined.export(SOUNDS_PATH + "/AudioOutput/" + str(n1) + "_" + str(n2) + ".wav", format='wav')
    n = n + 1
    #play(combined)

