from pydub import AudioSegment
from pydub.playback import play
from pathlib import Path
import random
import os
import shutil
# Mostly background noise
# https://www.pacdv.com/sounds/voices-1.html

#alternative source
#https://www.thesoundarchive.com/

MYPATH = Path().absolute()

#Clears the AudioOutput folder ready for new outputs
for root, dirs, files in os.walk(str(MYPATH) + "/dataset/mix/"):
    for f in files:
        os.unlink(os.path.join(root, f))
    for d in dirs:
        shutil.rmtree(os.path.join(root, d))
for root, dirs, files in os.walk(str(MYPATH) + "/dataset/s1/"):
    for f in files:
        os.unlink(os.path.join(root, f))
    for d in dirs:
        shutil.rmtree(os.path.join(root, d))
for root, dirs, files in os.walk(str(MYPATH) + "/dataset/s2/"):
    for f in files:
        os.unlink(os.path.join(root, f))
    for d in dirs:
        shutil.rmtree(os.path.join(root, d))

n = 0
soundArray = []
while n < 100:

    n1 = random.randint(1, 20)
    n2 = random.randint(1, 20)
    stringSound = str(n1) + "_" + str(n2)

    while stringSound in soundArray or n1 >= n2:
        n1 = random.randint(1, 20)
        n2 = random.randint(1, 20)
        if n1 > n2:
            w = n1
            n1 = n2
            n2 = w
        stringSound = str(n1) + "_" + str(n2)


    sound1_string = str(n1) + ".wav"
    sound2_string = str(n2) + ".wav"

    sound1 = AudioSegment.from_file("AudioSamples/" + sound1_string)
    sound2 = AudioSegment.from_file("AudioSamples/" + sound2_string)


    sound_diff = len(sound2) - len(sound1)

    if sound_diff > 0:
        silenced_segment = AudioSegment.silent(duration=sound_diff)
        sound1 = sound1 + silenced_segment
        #sound2 = sound2[:len(sound1)]
    else:
        sound_diff = sound_diff * -1
        silenced_segment = AudioSegment.silent(duration=sound_diff)
        sound2 = sound2 + silenced_segment
        #sound1 = sound1[:len(sound2)]

    SOUNDS_PATH = str(MYPATH)

    combined = sound1.overlay(sound2)

    sound1 = sound1.set_frame_rate(8000)
    sound2 = sound2.set_frame_rate(8000)
    combined = combined.set_frame_rate(8000)

    combined.export(SOUNDS_PATH + "/dataset/mix/" + str(n1) + "_" + str(n2) + ".wav", format='wav')
    sound1.export(SOUNDS_PATH + "/dataset/s1/" + str(n1) + "_" + str(n2) + ".wav", format='wav')
    sound2.export(SOUNDS_PATH + "/dataset/s2/" + str(n1) + "_" + str(n2) + ".wav", format='wav')
    soundArray.append(str(n1) + "_" + str(n2))
    n = n + 1
    #play(combined)