import noisereduce as nr
# load data
from scipy.io import wavfile
from pydub import AudioSegment
import numpy as np

rate, data = wavfile.read("12_20_s2.wav")
# select section of data that is noise
noisy_part = data[0:]
# perform noise reduction
reduced_noise = nr.reduce_noise(audio_clip=data, noise_clip=noisy_part, prop_decrease=1.0,  verbose=True)
wavfile.write("filt2.wav", rate, reduced_noise.astype(np.float32))

sound = AudioSegment.from_wav("filt2.wav")

# but let's make him *very* loud
sound = sound + 15

# save the output
sound.export("louder.wav", "wav")